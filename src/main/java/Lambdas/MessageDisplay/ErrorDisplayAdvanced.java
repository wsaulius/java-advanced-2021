package Lambdas.MessageDisplay;

public interface ErrorDisplayAdvanced {
    void displayMessage(ErrorCodes codes);
}
