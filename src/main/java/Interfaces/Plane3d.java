package Interfaces;

public class Plane3d {

    public static void main(String[] args) {

        Cube cube = new Cube(3);

        cube.getArea();
        cube.getPerimeter();

        cube.getVolume();
    }
}
